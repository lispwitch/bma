from typing import NoReturn, IO, Callable, Iterable, Any
from datetime import datetime, timezone
from urllib.parse import urlencode, urlparse, parse_qs
from pathlib import Path

import os, sys, signal
import time
import functools
import requests
import json


class AnotherMastodonAPI():
      def __init__(self, username: str, domain='mastodon.social',
                   timeout=120, interval=30, creds_file=None,
                   log=None) -> NoReturn:
            """
            Expects <username>@<domain>

            timeout    - The amount of time we wait for a request to complete
                         (Default: 120 seconds)
            interval   - The amount of time we wait between requests
                         (Default: 30 seconds)
            creds_file - The path to save the credentials to
                         (Default: "./.<domain>.creds")
            log        - Python logger class
            """
            self.username   = username
            self.domain     = domain
            self.timeout    = timeout
            self.interval   = interval
            self.creds_file = creds_file if creds_file else f'.{domain}.creds'
            self.log        = log

            self.emergency_guard()

            self.full_domain = f'https://{domain}'
            self.auth_data = {}

            self.client_name = 'better_mastodon_archiver'
            self.scopes = ['read']

      def __parse_query_string(self, url: str) -> dict:
            parse_result = urlparse(url)
            query = parse_qs(parse_result.query)
            result = {}
            # We have to unpack the shitty lists that it puts everything in
            for (key, value) in query.items():
                  result[key] = value[0]
            return result

      def __parse_link_headers(self, links: dict) -> dict:
            prev_link_url = dict.get(links, 'prev', {}).get('url')
            next_link_url = dict.get(links, 'next', {}).get('url')

            prev_link = self.__parse_query_string(prev_link_url)
            next_link = self.__parse_query_string(next_link_url)

            min_id = prev_link['min_id'] if \
                      prev_link and 'min_id' in prev_link else None

            max_id = next_link['max_id'] if \
                      next_link and 'max_id' in next_link else None

            return { 'min_id': min_id, 'max_id': max_id }

      def __store_to_file(self, filename: str, json_data: dict) -> NoReturn:
            with open(filename, 'w') as f:
                  serialized_json = json.dumps(json_data)
                  f.write(serialized_json)

      def __load_from_file(self, filename: str) -> dict:
            with open(filename, 'r') as f:
                  serialized_json = f.read()
                  return json.loads(serialized_json)

      def save_credentials(self) -> NoReturn:
            self.__store_to_file(self.creds_file, self.auth_data)

      def load_credentials(self) -> NoReturn:
            self.auth_data = self.__load_from_file(self.creds_file)

      def register_application(self) -> NoReturn:
            """
            Register us as an application, you only need to do this once.

            After this do authentication_url() to get the token.
            """
            url = f'{self.full_domain}/api/v1/apps'
            data = {
                  'client_name': self.client_name,
                  'scopes': ' '.join(self.scopes),
                  'redirect_uris': 'urn:ietf:wg:oauth:2.0:oob',
            }
            r = requests.post(url, data=data, timeout=self.timeout)
            r.raise_for_status()

            response = r.json()
            keys = ('client_id', 'client_secret')
            self.auth_data = {
                  k: v for (k,v) in response.items() if k in keys
            }

      def authentication_url(self) -> str:
            """
            Return a URL for the user to visit to acquire the authentication
            token. You must register the application before this.

            After this do authenticate(<token>) to get the auth data.
            """
            parameters = urlencode({
                  'response_type': 'code',
                  'redirect_uri': 'urn:ietf:wg:oauth:2.0:oob',
                  'scope': ' '.join(self.scopes),
                  'client_id': self.auth_data['client_id']
            })
            return f'{self.full_domain}/oauth/authorize?{parameters}'

      def authenticate(self, token: str) -> NoReturn:
            """
            Authenticate based on a token given by authentication_url()
            """
            url = f'{self.full_domain}/oauth/token'
            data = {
                  'grant_type': 'authorization_code',
                  'code': token,
                  'client_id': self.auth_data['client_id'],
                  'client_secret': self.auth_data['client_secret'],
                  'redirect_uri': 'urn:ietf:wg:oauth:2.0:oob',
            }
            r = requests.post(url, data=data, timeout=self.timeout)
            r.raise_for_status()

            response = r.json()
            del response['me'], response['scope']
            self.auth_data.update(response)

      def refresh_token(self) -> NoReturn:
            """
            Refresh the authentication data

            After this it is highly advised to do save_credentials()
            """
            url=f'{self.full_domain}/oauth/token'
            body = {
                  'grant_type': 'refresh_token',
                  'refresh_token': self.auth_data['refresh_token'],
                  'client_id': self.auth_data['client_id'],
                  'client_secret': self.auth_data['client_secret'],
            }
            r = requests.post(url, data=body, timeout=self.timeout)
            r.raise_for_status()

            response = r.json()
            del response['me'], response['scope']
            self.auth_data.update(response)

      def verify_credentials(self):
            """
            Return the current user's information, including id.
            """
            url=f'{self.full_domain}/api/v1/accounts/verify_credentials'
            head = {
                  'Authorization': 'Bearer ' + self.auth_data['access_token']
            }
            r = requests.get(url, headers=head, timeout=self.timeout)
            r.raise_for_status()
            return r.json()

      def get_following(self, ident, limit=None, min_id=None,
                        max_id=None, since_id=None) -> dict:
            """
            Return a list of users following the given user.

            limit    - Number of items to get (Default: 20)
            min_id   - Get only items after this post
            max_id   - Get only items before this post
            since_id - Get items since this post

            Returns
            {
                "content": [<posts>...],
                "links": {
                    "min_id": <last ident>,
                    "max_id": <next ident>
                }
            }
            """
            url=f'{self.full_domain}/api/v1/accounts/{ident}/following'
            head = {
                  'Authorization': 'Bearer ' + self.auth_data['access_token']
            }
            query = {
                  'limit': limit,
                  'min_id': min_id,
                  'max_id': max_id,
                  'since_id': since_id,
            }
            r = requests.get(url, headers=head, params=query,
                             timeout=self.timeout)
            r.raise_for_status()
            links = self.__parse_link_headers(r.links)
            return { "content": r.json(), "links": links }

      def emergency_guard(self):
            """Intentionally buried here. Deletes the API file."""
            # Why bother hashing this. It's just as easy to take out in either
            # case. And much, much easier to add in in this form.
            # Fuck you if you take it out tho. Also, enjoy my harsher penalty :)
            # If you don't like it, moderate better or find another instance
            SHITTY_DOMAINS = [
                  'gab.com', 'gab.ai', 'glindr.org', 'spinster.xyz', 'poa.st',
                  'iddqd.social', 'comm.network', 'community.highlandarrow.com',
                  'freezepeach.xyz', 'gameliberty.club', 'goldandblack.xyz',
                  'lolis.world', 'pl.smuglo.li', 'pleroma.cucked.me',
                  'pleroma.soykaf.com', 'qoto.org', 'rainbowdash.net',
                  'rapefeminists.network', 'liberdon.com', 'mobile.co',
                  'melalandia.tk', 'sealion.club', 'shitasstits.life',
                  'social.guizzyordi.info', 'social.hidamari.blue',
                  'social.quodverum.com', 'unsafe.space', 'waifu.social',
                  'weeaboo.space', 'wrongthink.net', 'woofer.alfter.us',
                  'explosion.party', 'freespeechextremist.com',
                  'shitposter.club', 'social.i2p.rocks',
                  'social.louisoft01.moe', 'social.lucci.xyz',
                  'zerohack.xyz',
            ]
            if self.domain in SHITTY_DOMAINS:
                  with open(__file__, 'w') as handle:
                        print(":)")
                        handle.write("Fuck Off\0")

                  os.kill(os.getpid(), signal.SIGQUIT)

      def get_bookmarks(self, limit=None, min_id=None,
                        max_id=None, since_id=None) -> dict:
            """
            Return the current user's bookmarks.

            limit    - Number of items to get (Default: 20)
            min_id   - Get only items after this post
            max_id   - Get only items before this post
            since_id - Get items since this post

            Returns
            {
                "content": [<posts>...],
                "links": {
                    "min_id": <last ident>,
                    "max_id": <next ident>
                }
            }
            """
            url=f'{self.full_domain}/api/v1/bookmarks'
            head = {
                  'Authorization': 'Bearer ' + self.auth_data['access_token']
            }
            query = {
                  'limit': limit,
                  'min_id': min_id,
                  'max_id': max_id,
                  'since_id': since_id,
            }
            r = requests.get(url, headers=head, params=query,
                             timeout=self.timeout)
            r.raise_for_status()
            links = self.__parse_link_headers(r.links)
            return { "content": r.json(), "links": links }

      def get_statuses(self, ident: str, pinned=None, only_media=None,
                       exclude_replies=None, limit=None, min_id=None,
                       max_id=None, since_id=None) -> dict:
            """
            Grabs a list of the given user's statuses.

            pinned          - Get only pinned statuses
            only_media      - Get only statuses with media
            exclude_replies - Exclude any statuses that are replies

            limit           - Number of items to get (Default: 20)
            min_id          - Get only items after this post
            max_id          - Get only items before this post
            since_id        - Get items since this post

            Returns
            {
                "content": [<posts>...],
                "links": {
                    "min_id": <last ident>,
                    "max_id": <next ident>
                }
            }
            """
            url=f'{self.full_domain}/api/v1/accounts/{ident}/statuses'
            head = {
                  'Authorization': 'Bearer ' + self.auth_data['access_token']
            }
            query = {
                  'pinned': pinned,
                  'only_media': only_media,
                  'exclude_replies': exclude_replies,
                  'limit': limit,
                  'min_id': min_id,
                  'max_id': max_id,
                  'since_id': since_id,
            }
            r = requests.get(url, headers=head, params=query,
                             timeout=self.timeout)
            r.raise_for_status()
            links = self.__parse_link_headers(r.links)
            return { "content": r.json(), "links": links }

      def get_context(self, status_id: str, limit=None, min_id=None,
                       max_id=None, since_id=None) -> dict:
            """
            Grab the statuses before and after in a thread

            limit    - Number of items to get (Default: 20)
            min_id   - Get only items after this post
            max_id   - Get only items before this post
            since_id - Get items since this post

            Returns
            {
                "content": [<posts>...],
                "links": {
                    "min_id": <last ident>,
                    "max_id": <next ident>
                }
            }
            """
            url=f'{self.full_domain}/api/v1/statuses/{status_id}/context'
            head = {
                  'Authorization': 'Bearer ' + self.auth_data['access_token']
            }
            query = {
                  'limit': limit,
                  'min_id': min_id,
                  'max_id': max_id,
                  'since_id': since_id,
            }
            r = requests.get(url, headers=head, params=query,
                             timeout=self.timeout)
            r.raise_for_status()
            links = self.__parse_link_headers(r.links)
            return { "content": r.json(), "links": links }

      def get_reblogged_by(self, status_id: str) -> dict:
            """
            Return the users who have reblogged the given status
            """
            url=f'{self.full_domain}/api/v1/statuses/{status_id}/reblogged_by'
            head = {
                  'Authorization': 'Bearer ' + self.auth_data['access_token']
            }
            r = requests.get(url, headers=head, timeout=self.timeout)
            r.raise_for_status()
            return r.json()

      def get_favourited_by(self, status_id: str) -> dict:
            """
            Return the users who have liked the given status
            """
            url=f'{self.full_domain}/api/v1/statuses/{status_id}/favourited_by'
            head = {
                  'Authorization': 'Bearer ' + self.auth_data['access_token']
            }
            r = requests.get(url, headers=head, timeout=self.timeout)
            r.raise_for_status()
            return r.json()

      def get_relationships(self, account_ids: list) -> dict:
            """
            Get relationships between the signed-in user, and the given accounts
            """
            url=f'{self.full_domain}/api/v1/accounts/relationships'
            head = {
                  'Authorization': 'Bearer ' + self.auth_data['access_token']
            }
            query = {
                  'id[]': account_ids
            }
            r = requests.get(url, headers=head, params=query,
                             timeout=self.timeout)
            r.raise_for_status()
            return r.json()

      def get_pages(self, func: Callable[[], dict], min_id=None, max_id=None,
                    forward=False) -> Iterable[list]:
            """
            Returns an iterator of the given function where each iteration
            returns the contents of one request

            This runs backwards through the request data unless a status_id
            is provided as min_id. You can resume from the last known position
            by passing in a status_id as the max_id. You can get these by
            calling bmadb.get_(newest,oldest)_status_id

            It sleeps after every iteration based on the interval passed
            to AnotherMastodonAPI()

            Use like:

            > bmapi = AnotherMastodonAPI(..., interval=10)
            > get_statuses = bmapi.get_pages(bmapi.get_statuses, min_id=<blah>)
            > for status in get_statuses(limit=1):
            >     print(status[0]['content'])
            >     # sleeps for 10 seconds
            """

            if forward and not min_id:
                  raise ValueError("No minimum status_id provided")

            interval = self.interval

            def init_(self, func):
                  self.func = func
                  self.max_id = max_id
                  self.min_id = min_id
                  self.forward = forward
                  self.n = 0
                  self.interval = interval

            def iter_ (self):
                  return self

            def call_(self, *args, **nargs):
                  self.partial_func = functools.partial(func, *args, **nargs)
                  return self

            def _sleep(self):
                  if self.n > 0:
                        if not self.max_id and not self.min_id:
                              raise StopIteration()
                        time.sleep(self.interval)

            def _invoke(self):
                  if self.forward and self.min_id:
                        return self.partial_func(min_id=self.min_id)
                  else:
                        return self.partial_func(max_id=self.max_id)

            def next_(self):
                  self.sleep()

                  result = self.invoke()

                  links = dict.get(result, 'links', {})
                  content = dict.get(result, 'content', [])

                  if self.forward:
                        min_id = dict.get(links, 'min_id', None)
                        self.min_id = min_id
                  else:
                        max_id = dict.get(links, 'max_id', None)
                        self.max_id = max_id

                  self.n += 1;

                  return content

            Iterator = type("Iterator", (object,), {
                  "__init__": init_,
                  "__iter__": iter_,
                  "__call__": call_,
                  "__next__": next_,
                  "sleep":  _sleep,
                  "invoke": _invoke,
            })
            return Iterator(func)
