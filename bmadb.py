from datetime import datetime, timezone
from typing import NoReturn, Optional
import sqlite3
import json

class AnotherMastodonArchiverDB():
    def __init__(self, filename: str, log: object) -> NoReturn:
        """
        Interfaces with a sqlite3 database 'archive'.

        filename  - The filename of the database to use
        log       - Python logger class
        """
        self.filename = filename
        self.log = log
        self.db = sqlite3.connect(filename)
        self.cursor = self.db.cursor()

    def setup_database(self) -> NoReturn:
        """
        Initialize the tables we need.

        statuses - contains status data
          status_id         - id of the status (primary key)
          account_id        - id of the account that posted the status
          reply_to          - id of the status this is replying to
          created_at        - ISO 8601 creation datetime of the status
          data              - JSON status data we get from fedi
          favourites        - JSON favourite data returned by /favourited_by
          boosts            - JSON boost data returned by /reblogged_by
          mentions          - JSON mentions data for the status
          is_direct_message - Boolean stating if it is a direct message
          is_pinned         - Boolean: if it is pinned
          is_reblog         - Boolean: if it is a boost of another message
          has_media         - Boolean: if it has a media attachment

        accounts - contains account data
          account_id   - id of the account (primary key)
          data         - JSON account data we get from fedi
          relationship - JSON relationship data returned by /relationships
        """
        try:
            self.cursor.execute(
                '''
                create table statuses
                    (status_id text primary key not null,
                    account_id text,
                    reply_to text,
                    created_at text not null,
                    data text not null,
                    favourites text,
                    boosts text,
                    mentions text,
                    is_direct_message boolean,
                    is_pinned boolean,
                    is_reblog boolean,
                    has_media boolean);
                '''
            )
            self.cursor.execute(
                '''
                create table accounts
                    (account_id text primary key not null,
                    data text,
                    relationship text
                    );
                ''')
            self.db.commit()
        except Exception as e:
            self.log.warning(f"{e}")
            pass

    def finish(self) -> NoReturn:
        """
        Commits and closes everything up
        """
        self.db.commit()
        self.db.close()


    def insert_status(self, status: dict, commit=True) -> NoReturn:
        """
        Insert a status into the statuses table

        commit  -  Whether or not we should commit the contents immediately

        This does not fill the fields 'favourites' or 'boosts', which require
        more API calls to fill.
        """
        # Primary key
        status_id  = dict.get(status, 'id', None)
        account_id = dict.get(status, 'account', {}).get('id', None)
        reply_to   = dict.get(status, 'in_reply_to_id', None)
        created_at = dict.get(status, 'created_at', None)

        data       = json.dumps(status)

        mentions   = json.dumps(dict.get(status, 'mentions', []))
        is_pinned  = dict.get(status, 'pinned', False)
        is_reblog  = dict.get(status, 'reblogged', False)
        has_media  = (len(dict.get(status, 'media_attachments', [])) > 0)

        privacy    = dict.get(status, 'privacy')
        is_direct_message = True if privacy == 'direct' else False

        values = (status_id, account_id, reply_to, created_at, data,
                  mentions, is_direct_message, is_pinned, is_reblog,
                  has_media)

        try:
            self.cursor.execute(
                '''
                insert into statuses
                    (status_id, account_id, reply_to, created_at, data,
                     mentions, is_direct_message, is_pinned, is_reblog,
                     has_media)
                values (?,?,?,?,?,?,?,?,?,?)
                ''',
                values
            )
        except sqlite3.IntegrityError as e:
            error_data = status_id if status_id else status
            self.log.warning(f'{e}: \'{error_data}\'')
            pass
        except Exception as e:
            self.log.warning(f'{e}: \'{status}\'')
            pass
        else:
            if commit:
                self.db.commit()
            self.log.info(f'Inserted status {status_id} into database.')

    def insert_statuses(self, statuses: list) -> NoReturn:
        """
        Insert a list of status into the statuses table

        This does not fill the fields 'favourites' or 'boosts', which require
        more API calls to fill.
        """
        for status in statuses:
            self.insert_status(status, commit=False)

        self.db.commit()

    def insert_favourites(self, status_id: str, favourites: list,
                          commit=True) -> NoReturn:
        """
        Update the 'favourites' column for status_id with the json data returned
        by /favourites
        """
        arguments = { 'favourites': json.dumps(favourites),
                      'status_id': status_id }
        try:
            self.cursor.execute(
                '''
                update statuses
                set favourites = :favourites
                where status_id = :status_id
                ''',
                arguments
            )
        except sqlite3.IntegrityError as e:
            error_data = status_id if status_id else favourites
            self.log.warning(f'{e}: \'{error_data}\'')
            pass
        except Exception as e:
            self.log.warning(f'{e}: \'{status_id}\', \'{favourites}\'')
        else:
            if commit:
                self.db.commit()
            self.log.info(f'Inserted favourites for {status_id} into database')

    def insert_boosts(self, status_id: str, boosts: list,
                      commit=True) -> NoReturn:
        """
        Update the 'favourites' column for status_id with the json data returned
        by /favourites
        """
        arguments = { 'boosts': json.dumps(boosts), 'status_id': status_id }
        try:
            self.cursor.execute(
                '''
                update statuses
                set boosts = :boosts
                where status_id = :status_id
                ''',
                arguments
            )
        except sqlite3.IntegrityError as e:
            error_data = status_id if status_id else boosts
            self.log.warning(f'{e}: \'{error_data}\'')
            pass
        except Exception as e:
            self.log.warning(f'{e}: \'{status_id}\', \'{boosts}\'')
            pass
        else:
            if commit:
                self.db.commit()
            self.log.info(f'Inserted boosts for {status_id} into database')

    def insert_account(self, account: dict, commit=True) -> NoReturn:
        """
        Insert an account into the accounts table.

        Does not fill the 'relationships' column which require more API calls
        to fill.
        """
        account_id = dict.get(account, 'id', None)
        data       = json.dumps(account)

        values = (account_id, data)
        try:
            self.cursor.execute(
                '''
                insert into accounts
                    (account_id, data)
                values (?,?)
                ''',
                values
            )
        except sqlite3.IntegrityError as e:
            error_data = account_id if account_id else account
            self.log.warning(f'{e}: \'{error_data}\'')
            pass
        except Exception as e:
            self.log.warning(f'{e}: \'{account_id}\', \'{account}\'')
            pass
        else:
            if commit:
                self.db.commit()
            self.log.info(f'Inserted account {account_id} into database')

    def insert_accounts(self, accounts: list) -> NoReturn:
        """
        Insert a list of accounts into the accounts table

        Does not fill the 'relationships' column which require more API calls
        to fill.
        """
        for status in statuses:
            self.insert_status(status, commit=False)

        self.db.commit()

    def total_statuses(self):
        """
        Returns the current number of statuses present in the database
        """
        try:
            self.cursor.execute(
                '''
                select count(*)
                from statuses;
                '''
            )
        except Exception as e:
            pass
        finally:
            result = self.cursor.fetchall()
            if len(result) > 0:
                return result[0][0]
            return 0

    def get_status_id(self, status_id: str) -> list:
        """
        Get stored data for status_id
        """
        try:
            self.cursor.execute(
                '''
                select *
                from statuses
                where status_id = (?)
                ''',
                (status_id,)
            )
        except Exception as e:
            self.log.warning(f'{e}')
            pass
        finally:
            return self.cursor.fetchall()


    def find_status_with_missing_field(self, field_name: str) -> Optional[list]:
        """
        Return a list of status_ids that do not have data for the given column
        """
        try:
            self.cursor.execute(
                '''
                select status_id
                from statuses
                where (?) is null
                ''',
                (field_name,)
            )
        except Exception as e:
            self.log.warning(f'{e}: \'{field_name}\'')
            pass
        finally:
            return self.cursor.fetchall()

    def find_oldest_status_id(self) -> str:
        """
        Return the status_id of the oldest status_id
        """
        try:
            self.cursor.execute(
                '''
                select status_id
                from statuses
                order by created_at asc
                limit 1;
                '''
            )
        except Exception as e:
            self.log.warning(f'{e}')
            raise
        finally:
            return self.cursor.fetchall()

    def find_newest_status_id(self) -> str:
        """
        Return the status_id of the newest status_id
        """
        try:
            self.cursor.execute(
                '''
                select status_id
                from statuses
                order by created_at desc
                limit 1;
                '''
            )
        except Exception as e:
            self.log.warning(f'{e}')
            raise
        finally:
            return self.cursor.fetchall()
