from bma import AnotherMastodonAPI
from bmadb import AnotherMastodonArchiverDB
import logging

bmapi = AnotherMastodonAPI('alexandria', 'catgirl.science', interval=30)
bmadb = AnotherMastodonArchiverDB('alexandria-catgirl.science.db', logging)

# Load credentials and grab current user data, including ID
# If there are no credentials stored you need to use bmapi to get some
bmapi.load_credentials()
my_account = bmapi.verify_credentials()
ident = my_account['id']

def scrape():
    bmadb.setup_database()
    total = bmadb.total_statuses()
    resume_id = bmadb.find_oldest_status_id()
    get_statuses = bmapi.get_pages(bmapi.get_statuses, max_id=resume_id)
    for item in get_statuses(ident, limit=20):
        bmadb.insert_statuses(item)
        n = len(item)
        total += n
        print(f'Inserted {n} statuses; total {total}')


scrape()
bmadb.finish()
